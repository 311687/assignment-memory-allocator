#define _DEFAULT_SOURCE

#include "test.h"
#include <unistd.h>
#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug(const char *fmt, ...);
static void* init_heap();
static void create_new_heap(struct block_header *last_block);
static struct block_header* block_frm_cont(void * data);

static const size_t HEAP_SIZE = 10000;

void test1(struct block_header *first_block) {
    void *data = _malloc(1000);
    if (data == NULL) {
        err("Тест 1 провален. _malloc вернул NULL.");
    }
    debug_heap(stdout, first_block);
    if (first_block->is_free != false || first_block->capacity.bytes != 1000) {
        err("Тест 1 провален. capacity или is_free не корректны.");
    }
    _free(data);
}

void test2(struct block_header *first_block) {
    void *data1 = _malloc(1000);
    void *data2 = _malloc(1000);
    if (data1 == NULL || data2 == NULL) {
        err("Тест 2 провален. _malloc вернул NULL.");
    }
    _free(data1);
    debug_heap(stdout, first_block);
    struct block_header *data1_block = block_frm_cont(data1);
    struct block_header *data2_block = block_frm_cont(data2);
    if (data1_block->is_free == false) {
        err("Тест 2 провален. Принимается статус 'free' освобождённого блока.");
    }
    if (data2_block->is_free == true) {
        err("Тест 2 провален. Статус 'free' - неовобожденный блок оказался освобождённым.");
    }
    _free(data1);
    _free(data2);
}

void test3(struct block_header *first_block) {
    void *data1 = _malloc(1000);
    void *data2 = _malloc(1000);
    void *data3 = _malloc(1000);
    if (data1 == NULL || data2 == NULL || data3 == NULL) {
        err("Тест 3 провален. _malloc вернул NULL.");
    }
    _free(data2);
    _free(data1);
    debug_heap(stdout, first_block);
    struct block_header *data1_block = block_frm_cont(data1);
    struct block_header *data3_block = block_frm_cont(data3);
    if (data1_block->is_free == false) {
        err("Тест 3 провален. Статус 'free' - неовобожденный блок оказался освобождённым.");
    }
    if (data3_block->is_free == true) {
        err("Тест 3 провален. Статус 'free' - неосвобождённый блок оказался свободным.");
    }
    if (data1_block->capacity.bytes != 2000 + offsetof(struct block_header, contents)) {
        err("Тест 3 провален. Неверный размер блока.");
    }
    _free(data1);
    _free(data2);
    _free(data3);
}

void test4(struct block_header *first_block) {
    void *data1 = _malloc(10000);
    void *data2 = _malloc(10000);
    void *data3 = _malloc(5000);
    if (data1 == NULL || data2 == NULL || data3 == NULL) {
        err("Тест 4 провален. _malloc вернул NULL.");
    }
    _free(data2);
    _free(data3);
    debug_heap(stdout, first_block);
    struct block_header *data1_block = block_frm_cont(data1);
    struct block_header *data2_block = block_frm_cont(data2);
    if ((uint8_t *)data1_block->contents + data1_block->capacity.bytes != (uint8_t*) data2_block){
        err("Тест 4 провален. _block не был создан рядом с первым созданным блоком.");
    }
    _free(data1);
    _free(data2);
    _free(data3);
}

void test5(struct block_header *first_block) {
    void *data1 = _malloc(10000);
    if (data1 == NULL) {
        err("Тест 5 провален. _malloc вернул NULL.");
    }
    struct block_header *addr = first_block;
    while (addr->next != NULL) addr = addr->next;
    create_new_heap(addr);
    void *data2 = _malloc(100000);
    debug_heap(stdout, first_block);
    struct block_header *data2_block = block_frm_cont(data2);
    if (data2_block == addr) {
        err("Тест 5 провален. _created блок рядом с первым выделенным блоком heap.");
    }
    _free(data1);
    _free(data2);
}

void test() {
    struct block_header *first_block = (struct block_header*) init_heap();
    test1(first_block);
    test2(first_block);
    test3(first_block);
    test4(first_block);
    test5(first_block);
    debug("Тесты пройдены.\n");
}

static void* init_heap() {
    void *heap = heap_init(INITIAL_HEAP_SIZE);
    if (heap == NULL) {
        err("Невозможно инициализировать heap для тестов.");
    }
    return heap;
}

static void create_new_heap(struct block_header *last_block) {
    struct block_header *addr = last_block;
    void* test_addr = (uint8_t*) addr + size_from_capacity(addr->capacity).bytes;
    test_addr = mmap( (uint8_t*) (getpagesize() * ((size_t) test_addr / getpagesize() +
                                       (((size_t) test_addr % getpagesize()) > 0))), 1000,
          PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED_NOREPLACE,0, 0);
    debug(test_addr);
}

static struct block_header* block_frm_cont(void* data) {
    return (struct block_header *) ((uint8_t *) data - offsetof(struct block_header, contents));
}
